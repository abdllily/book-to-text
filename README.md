# book-to-text

Transforms a folder of tif images (representing pages of a book) to text using tesseract OCR. This will remove line breaks besides paragraphs and rejoin words which are split across pages.

# Requirements:

- Tesseract + any language packs.
- Python3

# Licence

GPL v3.
