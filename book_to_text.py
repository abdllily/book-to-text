#!/usr/bin/python3
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import argparse
import subprocess
import string
import glob
import os

class Page:

    def __init__(self, filename, prefix, language):
        self.filename = filename
        self.prefix = prefix
        self.language = language

    @property
    def page_number(self):
        filename = self.filename.split("/")[-1][len(self.prefix):]
        if "_" in filename:
          scan, page = filename.split("_")
          page = int(page.split(".", 1)[0][:-1])
          scan = int(scan)
        else:
          scan, page = int(filename[:-4]), 1

        return scan, page

    def ocr(self):
        ocr_proc = subprocess.Popen(
            ("tesseract", "-l", self.language, self.filename, "stdout"),
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE
        )
        ocr_proc.wait()
        return ocr_proc.stdout.read().decode("utf-8")

    def ocr_reformatted(self):
        page = self.ocr()
        reformatted = []
        current_line = []
        for line in page.split("\n"):
            if line == "":
                reformatted.append(" ".join(current_line))
                current_line = []
            elif line in string.whitespace:
                continue
            elif current_line:
                # If a word is split over a line, rejoin it.
                last = current_line[-1]
                if last.endswith("-") and not last.endswith(" -"):
                    current_line[-1] = last[:-1] + line.split()[0]
                    if len(line.split()) > 1:
                        current_line.append(" " + " ".join(line.split()[1:]))
                    else:
                        current_line.append(line)
                else:
                    current_line.append(line)
            else:
                current_line.append(line)

        if current_line:
            reformatted.append(" ".join(current_line))

        return "\n\n".join(reformatted)

    def __lt__(self, other):
        scan, page = self.page_number
        oscan, opage = other.page_number
        if scan < oscan:
            return True
        elif scan == oscan:
            return page < opage
        return False

    def __str__(self):
        return "<Page {0} side {1}>".format(*self.page_number)

    def __repr__(self):
        return str(self)

def rejoin_cross_page_words(page1, page2=None):
    """ If the last word of page1 is split over two pages, rejoin them. """
    if page1.endswith("-"):
        p2_first_word, page2 = tuple(page2.split(" ", 1))
        page1 = page1[:-1] + p2_first_word

    return page1, page2

def write_page(number, name_format, text):
    fout = open(name_format.format(number), "w")
    fout.write(text.encode("utf-8"))
    fout.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="OCR and cleans up text.")
    parser.add_argument("directory", type=str, help="Directory of .tif files")
    parser.add_argument(
        "prefix",
        type=str,
        help="Prefix used for tif files."
    )
    parser.add_argument(
        "--output-format",
        type=str,
        default="Formatted_{0}.txt",
        help="Python formatted string to use as destination file format."
    )
    parser.add_argument(
        "--quiet",
        default=False,
        help="Don't display anything but errors.",
        action="store_true"
    )
    parser.add_argument(
        "--output-directory",
        type=str,
        default=os.getcwd(),
        help="Directory to store the output"
    )
    parser.add_argument(
        "--language",
        type=str,
        default="en",
        help="Specify the language of the book for OCR."
    )
    args = parser.parse_args()

    if os.path.isdir(args.output_directory) is False:
        os.mkdir(args.output_directory)

    pages = []
    for f in glob.glob(args.directory + "*.tif"):
        pages.append(Page(f, args.prefix, args.language))
    pages.sort()

    # Okay we're going to go through the pages one by one and try to reformat the
    # text but we may need to look ahead for when joining across pages.
    final = {}
    for number, page in enumerate(pages):
        if args.quiet is False:
            print("Processing page: {0} ({1})".format(page.filename, number+1))

        if number not in final:
            final[number] = page.ocr_reformatted()
        ctext = final[number]

        if len(pages) <= number+1:
            # We're actually on the last page.
            final[number+1] = None
        else:
            final[number+1] = pages[number+1].ocr_reformatted()
        ntext = final[number+1]

        final[number], final[number+1] = rejoin_cross_page_words(ctext, ntext)
        write_page(
            number+1,
            args.output_directory + "/" + args.output_format,
            final[number]
        )

    print("All done!")
